#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

static bool grid[256][256];

struct s_point
{
    int32_t x;
    int32_t y;
};

static bool move(struct s_point *p, char c)
{
    bool res = true;
    if (p == NULL)
    {
        res = false;
    }
    else
    {
        if (c == '^')
        {
            p->x++;
        }
        else if (c == 'v')
        {
            p->x--;
        }
        else if (c == '>')
        {
            p->y++;
        }
        else if (c == '<')
        {
            p->y--;
        }

        if (p->x > 255)
        {
            fprintf(stderr, "X+ out of range %" PRId32 "\n", p->x);
            res = false;
        }
        else if (p->x < 0)
        {
            fprintf(stderr, "X+ out of range %" PRId32 "\n", p->x);
            res = false;
        }
        else if (p->y > 255)
        {
            fprintf(stderr, "Y out of range %" PRId32 "\n", p->y);
            res = false;
        }
        else if (p->y < 0)
        {
            fprintf(stderr, "Y out of range %" PRId32 "\n", p->y);
            res = false;
        }
        else
        {
            res = true;
        }
    }

    return res;
}

static int32_t Process(FILE * in)
{
    bool res = true;
    uint32_t count = 1;
    struct s_point robot = { 256/2, 256/2};
    struct s_point santa = { 256/2, 256/2};
    struct s_point *p = &robot;

    memset(grid, false, sizeof(grid));

    grid[256/2][256/2] = true;
    // fprintf(stdout, "%" PRId32 ",%" PRId32 "==> %d\n", x, y, grid[x][y]);

    while (!feof(in) && res)
    {
        int c = fgetc(in);

        if (p == &robot)
        {
            p = &santa;
        }
        else
        {
            p = &robot;
        }

        res = move(p, c);

        if (res)
        {
            // fprintf(stdout,  "%c | %" PRId32 ",%" PRId32 "==> %d\n",c, x, y, grid[x][y]);
            if (grid[p->x][p->y] == false)
            {
                grid[p->x][p->y] = true;
                count++;
            }
        }
    }

    fprintf (stdout, "Count = %" PRIu32 "\n", count);

    return count;
}



int main(int, char**){
    printf("Hello, from AventOfCode!\n");

    FILE * f = fopen("../Day03/test1.1.txt", "r");
    if (f != NULL)
    {
        Process(f);
        fclose(f);
    }
    f = fopen("../Day03/test2.txt", "r");
    if (f != NULL)
    {
        Process(f);
        fclose(f);
    }

    f = fopen("../Day03/test3.txt", "r");
    if (f != NULL)
    {
        Process(f);
        fclose(f);
    }

    f = fopen("../Day03/INPUT.txt", "r");
    if (f != NULL)
    {
        Process(f);
        fclose(f);
    }

    return EXIT_SUCCESS;
}
