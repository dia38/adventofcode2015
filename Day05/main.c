#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

const char voyelle[] = "aeiou";


static int32_t Process(FILE * in)
{
    bool res = true;
    uint32_t count = 0;
    char old = 0;

    uint32_t nbVoyelle = 0;
    uint32_t nbDouble = 0;
    uint32_t nbBad = 0;

    while (!feof(in))
    {
        int c = fgetc(in);

        if (c == '\n')
        {
            if (nbBad > 0)
            {

            }
            else if (nbVoyelle < 3)
            {

            }
            else if (nbDouble == 0)
            {

            }
            else
            {
                count++;
            }

            nbVoyelle = 0;
            nbDouble = 0;
            nbBad = 0;
            old = 0;
        }
        else 
        {
            char *ca = strchr(voyelle, c);
            if (ca != NULL)
            {
                nbVoyelle++;
            }
            
            if ( old == c )
            {
                nbDouble++;
            }
            else if ( old == 'a' && c == 'b')
            {
                nbBad++;
            }
            else if ( old == 'c' && c == 'd')
            {
                nbBad++;
            }
            else if ( old == 'p' && c == 'q')
            {
                nbBad++;
            }
            else if ( old == 'x' && c == 'y')
            {
                nbBad++;
            }

            old = c;
        }
    }

    if (nbBad > 0)
    {
        fprintf(stderr, "Nb bad (%d) \n", nbBad);
    }
    else if (nbVoyelle < 3)
    {
        fprintf(stderr, "Nb voyelle (%d) \n", nbVoyelle);
    }
    else if (nbDouble == 0)
    {
        fprintf(stderr, "Nb double (%d) \n", nbDouble);
    }
    else
    {
        count++;
    }

    fprintf (stdout, "Count = %" PRIu32 "\n", count);

    return count;
}

static bool TestR1(char *string, size_t size)
{
    char pattern[3] = {string[0], string[1], 0};
    char *Found =NULL;

    string[size] = 0;

    for(int i = 2; (i < size) && (Found == NULL) ; i++)
    {
        Found = strstr(string + i, pattern);
        pattern[0] = pattern[1];
        pattern[1] = string[i];
    }

    return Found != NULL;
}


static int32_t Process2(FILE * in)
{
    bool res = true;
    uint32_t count = 0;
    uint32_t nbR2 = 0;
    char old[3] = {0};

    char    string[32];
    size_t  size = 0;

    while (!feof(in))
    {
        int c = fgetc(in);

        string[size++] = c;

        old[2] = old[1];
        old[1] = old[0];
        old[0] = c;

        if (c == '\n')
        {
            if (nbR2 == 0)
            {

            }
            else 
            {
                bool nbR1 = TestR1(string, size);
                if (nbR1)
                {
                    count++;
                }
            }

            nbR2 = 0;
            size = 0;
            memset(old, 0, sizeof(old));
        }
        else 
        {
            if (old[2] == old[0])
            {
                nbR2++;
            }
        }
    }

    fprintf (stdout, "Count = %" PRIu32 "\n", count);

    return count;
}



int main(int, char**){
    printf("Hello, from AventOfCode!\n");

    FILE * f = fopen("../Day05/Test01.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }
    f = fopen("../Day05/Test02.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }

    f = fopen("../Day05/Test03.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }

    f = fopen("../Day05/Test04.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }

    f = fopen("../Day05/Test05.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }


    f = fopen("../Day05/INPUT.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }

    return EXIT_SUCCESS;
}
