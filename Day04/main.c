#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <openssl/md5.h>

// La donnée fixe à hacher
char* message = "yzbqklnj";

// La fonction qui calcule le hachage md5 d'une chaîne
char* md5_hash(char* input) {
    int i;
    MD5_CTX c;
    unsigned char digest[16];
    char* output = (char*)malloc(33);

    MD5_Init(&c);
    MD5_Update(&c, input, strlen(input));
    MD5_Final(digest, &c);

    for (i = 0; i < 16; i++) {
        sprintf(&output[i*2], "%02x", (unsigned int)digest[i]);
    }

    return output;
}

// La fonction qui vérifie si le hachage commence par 4 zéros
int check_hash(char* hash) {
    return strncmp(hash, "000000", 6) == 0;
}

int main() {
    char* input;
    char* hash;
    int nonce = -1;
    clock_t start, end;
    double cpu_time_used;

    // Initialisation du générateur aléatoire
    srand(time(NULL));

    // Démarrage du chronomètre
    start = clock();

    // Boucle principale
    while (1) {
        // Génération aléatoire du nonce
        nonce++;

        // Concaténation du message et du nonce
        input = (char*)malloc(strlen(message) + 12);
        sprintf(input, "%s%d", message, nonce);

        // Calcul du hachage md5
        hash = md5_hash(input);

        // Vérification du hachage
        if (check_hash(hash)) {
            // Arrêt du chronomètre
            end = clock();
            cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

            // Affichage du résultat
            printf("Nonce: %d\n", nonce);
            printf("Hash: %s\n", hash);
            printf("Time: %f seconds\n", cpu_time_used);

            // Libération de la mémoire
            free(input);
            free(hash);

            // Sortie de la boucle
            break;
        }

        // Libération de la mémoire
        free(input);
        free(hash);
    }

    return 0;
}
