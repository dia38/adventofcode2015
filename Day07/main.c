#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

typedef enum
{
    NONE,
    AND,
    OR,
    LSHIFT,
    RSHIFT,
    NOT,
    VALUE,
} te_op;

typedef struct s_noeud
{
    char            name[16];
    struct s_noeud *left;
    struct s_noeud *right;
    te_op           op;
    uint16_t        value;
} ts_node;

static ts_node circuit[512];

static ts_node *find_node(char *name)
{
    ts_node *free = NULL;
    ts_node *find = NULL;

    for (int i = 0 ; (i < 512) && (find == NULL ) ; i++)
    {
        if ((circuit[i].op == NONE))
        {
            if (free == NULL)
            {
                free = &circuit[i];
            }
        }
        else
        {
            int r = strcmp(circuit[i].name, name);
            if (r == 0)
            {
                find = &circuit[i];
            }
        }
    }

    if (find == NULL)
    {
        find = free;
    }

    return find;
}

static void Process(FILE *in)
{
    fpos_t curr;
    char left[16];
    char right[16];
    char result[16];

    memset(circuit, 0, sizeof(circuit));

    while (!feof(in))
    {
        int res = fgetpos(in, &curr);
        assert(res == 0);

        int n = 0;

        if (n == 0)
        {
            res = fsetpos(in, &curr);
            assert(res == 0);

            n = fscanf(in, "NOT %s -> %s\n", left, result);
            if (n == 2)
            {
                ts_node *r = find_node(result);
                assert(r != NULL);
                strcpy(r->name, result);
                r->op = NOT;
                r->left = find_node(left);
                assert(r->left != NULL);
                strcpy(r->left->name, left);
                if (r->left->op == NONE)
                {
                    r->left->op = VALUE;
                }
            }
            else
            {
                n = 0;
            }
        }

        if (n == 0)
        {
            res = fsetpos(in, &curr);
            assert(res == 0);

            n = fscanf(in, "%s -> %s\n", left, result);
            if (n == 2)
            {
                ts_node *r = find_node(result);
                assert(r != NULL);
                strcpy(r->name, result);
                r->op = VALUE;

                r->left = find_node(left);
                assert(r->left != NULL);
                strcpy(r->left->name, left);
                if (r->left->op == NONE)
                {
                    r->left->op = VALUE;
                    n = sscanf(left, "%" SCNu16, &r->left->value);
                }
            }
            else
            {
                n = 0;
            }
        }

        if (n == 0)
        {
            res = fsetpos(in, &curr);
            assert(res == 0);

            n = fscanf(in, "%s AND %s -> %s\n", left, right, result);
            if (n == 3)
            {
                ts_node *r = find_node(result);
                assert(r != NULL);
                strcpy(r->name, result);
                r->op = AND;
                r->left = find_node(left);
                assert(r->left != NULL);
                strcpy(r->left->name, left);
                if (r->left->op == NONE)
                {
                    r->left->op = VALUE;
                }
                r->right = find_node(right);
                assert(r->right != NULL);
                strcpy(r->right->name, right);
                if (r->right->op == NONE)
                {
                    r->right->op = VALUE;
                }
            }
            else
            {
                n = 0;
            }
        }

        if (n == 0)
        {
            res = fsetpos(in, &curr);
            assert(res == 0);

            n = fscanf(in, "%s OR %s -> %s\n", left, right, result);
            if (n == 3)
            {
                ts_node *r = find_node(result);
                assert(r != NULL);
                strcpy(r->name, result);
                r->op = OR;
                r->left = find_node(left);
                assert(r->left != NULL);
                strcpy(r->left->name, left);
                if (r->left->op == NONE)
                {
                    r->left->op = VALUE;
                }
                r->right = find_node(right);
                assert(r->right != NULL);
                strcpy(r->right->name, right);
                if (r->right->op == NONE)
                {
                    r->right->op = VALUE;
                }
            }
            else
            {
                n = 0;
            }
        }

        if (n == 0)
        {
            res = fsetpos(in, &curr);
            assert(res == 0);

            n = fscanf(in, "%s LSHIFT %s -> %s\n", left, right, result);

            if (n == 3)
            {
                ts_node *r = find_node(result);
                assert(r != NULL);
                strcpy(r->name, result);
                r->op = LSHIFT;
                r->left = find_node(left);
                assert(r->left != NULL);
                strcpy(r->left->name, left);
                if (r->left->op == NONE)
                {
                    r->left->op = VALUE;
                }

                r->right = find_node(right);
                assert(r->right != NULL);
                strcpy(r->right->name, right);
                if (r->right->op == NONE)
                {
                    r->right->op = VALUE;
                    n = sscanf(right, "%" SCNu16, &r->right->value);
                    assert(n == 1);
                }
            }
            else
            {
                n = 0;
            }
        }

        if (n == 0)
        {
            res = fsetpos(in, &curr);
            assert(res == 0);

            n = fscanf(in, "%s RSHIFT %s -> %s\n", left, right, result);

            if (n == 3)
            {
                ts_node *r = find_node(result);
                assert(r != NULL);
                strcpy(r->name, result);
                r->op = RSHIFT;
                r->left = find_node(left);
                assert(r->left != NULL);
                strcpy(r->left->name, left);
                if (r->left->op == NONE)
                {
                    r->left->op = VALUE;
                }

                r->right = find_node(right);
                assert(r->right != NULL);
                strcpy(r->right->name, right);
                if (r->right->op == NONE)
                {
                    r->right->op = VALUE;
                    n = sscanf(right, "%" SCNu16, &r->right->value);
                    assert(n == 1);
                }
            }
            else
            {
                n = 0;
            }
        }

        if (n == 0)
        {
            fprintf(stderr, "Parsser Not Found\n");
        }
    }
}

void Eval(void)
{
    bool con = true;

    while (con)
    {
        con = false;
        for (size_t i = 0; i < 512; i++)
        {
            switch (circuit[i].op)
            {
                case NONE:
                {

                }
                break;
                
                case AND:
                {
                    assert(circuit[i].right != NULL);
                    assert(circuit[i].left != NULL);
                    uint16_t v = circuit[i].right->value & circuit[i].left->value;

                    con |= (circuit[i].value != v);
                    circuit[i].value = v;
                }
                break;
                
                case OR:
                {
                    assert(circuit[i].right != NULL);
                    assert(circuit[i].left != NULL);
                    uint16_t v = circuit[i].right->value | circuit[i].left->value;

                    con |= (circuit[i].value != v);
                    circuit[i].value = v;
                }
                break;
                
                case RSHIFT:
                {
                    assert(circuit[i].right != NULL);
                    assert(circuit[i].left != NULL);
                    uint16_t v = circuit[i].left->value >> circuit[i].right->value;

                    con |= (circuit[i].value != v);
                    circuit[i].value = v;
                }
                break;
                
                case LSHIFT:
                {
                    assert(circuit[i].right != NULL);
                    assert(circuit[i].left != NULL);
                    uint16_t v = circuit[i].left->value << circuit[i].right->value;

                    con |= (circuit[i].value != v);
                    circuit[i].value = v;
                }
                break;
                
                case NOT:
                {
                    assert(circuit[i].left != NULL);
                    uint16_t v = ~circuit[i].left->value;

                    con |= (circuit[i].value != v);
                    circuit[i].value = v;
                }
                break;
                
                case VALUE:
                {
                    if (circuit[i].left != NULL)
                    {
                        uint16_t v = circuit[i].left->value;

                        con |= (circuit[i].value != v);
                        circuit[i].value = v;
                    }
                }
                break;
                
                default:
                    break;
            }
        }
    }
}

void Print(void)
{
    for (size_t i = 0; i < 512; i++)
    {
        if (circuit[i].op != NONE)
        {
            fprintf(stdout, "%s: %" PRIu16 "\n", circuit[i].name, circuit[i].value);
        }
    }
    
}

int main(int, char**)
{
    printf("Hello, from AventOfCode!\n");

    FILE * f = fopen("../Day07/test01.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Eval();
        Print();
        fclose(f);
    }

    f = fopen("../Day07/INPUT.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Eval();
        Print();
        fclose(f);
    }

    return EXIT_SUCCESS;
}
