#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

static uint16_t grid[1000][1000];

typedef struct
{
    uint32_t x;
    uint32_t y;
} ts_point;

void On(ts_point *start, ts_point *end)
{
    uint32_t x, y;

    for (x = start->x; x <= end->x; x++)
    {
        for (y = start->y; y <= end->y; y++)
        {
            grid[x][y]++;
        }
    }
}

void Off(ts_point *start, ts_point *end)
{
    uint32_t x, y;

    for (x = start->x; x <= end->x; x++)
    {
        for (y = start->y; y <= end->y; y++)
        {
            if (grid[x][y] > 0)
            {
                grid[x][y]--;
            }
        }
    }
}

void toggle(ts_point *start, ts_point *end)
{
    uint32_t x, y;

    for (x = start->x; x <= end->x; x++)
    {
        for (y = start->y; y <= end->y; y++)
        {
            grid[x][y] += 2;
        }
    }
}

static void Process(FILE *in)
{
    ts_point start;
    ts_point end;
    char action[10];

    memset(grid, 0, sizeof(grid));

    while (!feof(in))
    {
        int n = fscanf(in, "%s", action);

        if (n == 1)
        {
            if (strncmp("turn", action, sizeof(action)) == 0)
            {
                n = fscanf(in, "%s", action);
            }

            if (n != 1)
            {
                fprintf(stderr, "Error1 Parse %d\n", n);
                return;
            }
        }
        else
        {
            fprintf(stderr, "Error2 Parse %d\n", n);
            return;
        }

        n = fscanf(in, "%" SCNu32 ",%" SCNu32 " through %"  SCNu32 ",%" SCNu32 "\n",
            &start.x, &start.y, 
            &end.x, &end.y
        ); 

        if (n == 4)
        {
            if (strncmp("on", action, sizeof(action)) == 0)
            {
                On(&start, &end);
            }
            else if (strncmp("off", action, sizeof(action)) == 0)
            {
                Off(&start, &end);
            }
            else if (strncmp("toggle", action, sizeof(action)) == 0)
            {
                toggle(&start, &end);
            }
            else 
            {
                fprintf(stderr, "invalid action [%s]\n", action);
                return;
            }
        }
        else
        {
            fprintf(stderr, "Error Parse %d\n", n);
            return;
        }
    }
}

static uint32_t Count(void)
{
    uint32_t x, y;
    uint32_t count = 0;

    for (x =  0; x < 1000; x++)
    {
        for (y = 0; y < 1000; y++)
        {
            count += grid[x][y];
        }
    }

    fprintf(stdout, "Count %" PRIu32 "\n", count);

    return count;
}

int main(int, char**){
    printf("Hello, from AventOfCode!\n");

    FILE * f = fopen("../Day06/test1.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Count();
        fclose(f);
    }
    f = fopen("../Day06/test2.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Count();
        fclose(f);
    }

    f = fopen("../Day06/test3.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Count();
        fclose(f);
    }
     f = fopen("../Day06/test4.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Count();
        fclose(f);
    }

    f = fopen("../Day06/test5.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Count();
        fclose(f);
    }
    f = fopen("../Day06/INPUT.txt", "r");
    if (f != NULL)
    {
        Process(f);
        Count();
        fclose(f);
    }

    return EXIT_SUCCESS;
}
