#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

#define SIZE  1024

char mString[SIZE] = {0};


void doProcess(void)
{
    char   wRes[SIZE] = {0};
    uint32_t   wCount;

    for (wCount = 0 ; wCount < 40 ; wCount++)
    {
        char *in = mString;
        char *out = wRes;
        char c = *in;
        uint32_t nb = 0;

        memset(wRes, 0, sizeof(wRes));

        while (*in != 0)
        {
            if  (*in == c)
            {
                nb++;
            }
            else
            {
                out += sprintf(out, "%" PRIu32 "%c", nb, c);

                c = *in;
                nb = 1;
            }

            in++;
        }

        out += sprintf(out, "%" PRIu32 "%c", nb, c);
        strcpy(mString, wRes);

        fprintf(stdout, "%" PRIu32 " : %s\n", wCount, wRes);
    }
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t wStage1 = 0;

    while (!feof(input))
    {
        int c = fscanf(input, "%s\n", mString);
    }

    doProcess();

    return wStage1;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "Total visible : %" PRIu32 "\n", total);
        fclose(input);

    }

    return wres;
}
