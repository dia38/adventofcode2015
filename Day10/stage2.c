#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <inttypes.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>
#include <assert.h>

#define NB_VILLE  32

char mList[NB_VILLE][32];
uint32_t mDistance[NB_VILLE][32];
uint32_t mCount = 0;

bool     mPass[NB_VILLE];
uint32_t mMin = 0;

void Compute(uint32_t parSrc, uint32_t parDist)
{
    uint32_t wIdx;
    uint32_t wRes = 0;

    for (wIdx = 0; wIdx < mCount; wIdx++)
    {
        if (mPass[wIdx] == false)
        {
            wRes++;
            mPass[wIdx] = true;

            Compute(wIdx, parDist + mDistance[parSrc][wIdx]);
            mPass[wIdx] = false;
        }
    }
    
    if (wRes == 0)
    {
        if (parDist > mMin)
        {
            mMin = parDist;
        }
    }
}

void doProcess(void)
{
     uint32_t   wSrc;
     uint32_t   wDst;

    for (wSrc = 0 ; wSrc < mCount ; wSrc++)
    {
        memset(mPass, false, sizeof(mPass));

        mPass[wSrc] = true;
        Compute(wSrc, 0);
        mPass[wSrc] = false;
    }

    fprintf(stdout, "shortest route : %" PRIu32 "\n", mMin);
}

uint32_t getIndex(char *parName)
{
    uint32_t wRes = UINT32_MAX;
    uint32_t wIdx = 0;

    for (wIdx = 0 ; (wIdx < mCount) && (wRes == UINT32_MAX) ; wIdx++)
    {
        int c = strcmp(mList[wIdx], parName);
        if (c == 0)
        {
            wRes = wIdx;
        }
    }

    if (wRes == UINT32_MAX)
    {
        wRes = mCount++;
        strcpy(mList[wRes], parName);
    }

    return wRes;
}

uint32_t doProcessFile(FILE *input)
{
    uint32_t wStage1;

    char        wOrign[64];
    char        wDest[64];
    uint32_t    wLen;

    while (!feof(input))
    {
        int c = fscanf(input, "%s to %s = %" SCNu32 "\n", wOrign, wDest, &wLen);

        if (c == 3)
        {
            uint32_t wSrc = getIndex(wOrign);
            uint32_t wDst = getIndex(wDest);

            if (wSrc == UINT32_MAX)
            {

            }
            else if (wDst == UINT32_MAX)
            {

            }
            else
            {
                mDistance[wSrc][wDst] = wLen;
                mDistance[wDst][wSrc] = wLen;
            }
        }
    }

    doProcess();

    return mMin;
}

int main(
    int argc, 
    char* argv[]
)
{
    int wres = EXIT_FAILURE;
    FILE *input = NULL;
    if (argc > 1)
    {
        input = fopen(argv[1], "r");

        if (input != NULL)
        {
            wres = EXIT_SUCCESS;
        }
    }

    if (wres == EXIT_SUCCESS)
    {
        uint32_t total = 0;

        total = doProcessFile(input);

        fprintf(stdout, "Total visible : %" PRIu32 "\n", total);
        fclose(input);

    }

    return wres;
}
