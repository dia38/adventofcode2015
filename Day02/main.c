#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>


static int32_t Day01(FILE * in)
{
    uint32_t area = 0;
    uint32_t area_min = UINT32_MAX;
    uint32_t area_lw;
    uint32_t area_wh;
    uint32_t area_hl;
    uint32_t ruban_size;
    uint32_t ruban_tot = 0;
    uint32_t l, w, h;

    while (!feof(in))
    {
        int nb = fscanf(in, "%" SCNd32 "x%" SCNd32 "x%" SCNd32 "\n", &l, &w, &h  );

        if (nb == 3)
        {
            area_hl = h * l;
            area_lw = l * w;
            area_wh = w * h;

            area_min = area_hl;
            if (area_lw < area_min)
            {
                area_min = area_lw;
            }
            if (area_wh < area_min)
            {
                area_min = area_wh;
            }

            area += (2* area_hl + 2 * area_lw + 2 * area_wh + area_min);

            ruban_size = 2*h + 2 *l;

            if ((2*l + 2*w) < ruban_size)
            {
                ruban_size = (2*l + 2*w);
            }

            if ((2*w + 2*h) < ruban_size)
            {
                ruban_size = (2*w + 2*h);
            }

            ruban_size += (l * w * h);

            ruban_tot += ruban_size;
        }
        else
        {
            fprintf(stderr, "Invalid Read %d\n", nb);
        }
    }

    fprintf (stdout, "%" PRIu32 "x%" PRIu32 "x%" PRIu32 "= %" PRIu32 "\n", l, w, h, area);
    fprintf (stdout, "Ruban = %" PRId32 "\n", ruban_tot);

    return area;
}



int main(int, char**){
    printf("Hello, from AventOfCode!\n");

    FILE * f = fopen("../Day02/test1.txt", "r");
    if (f != NULL)
    {
        Day01(f);
        fclose(f);
    }
    f = fopen("../Day02/test2.txt", "r");
    if (f != NULL)
    {
        Day01(f);
        fclose(f);
    }

    f = fopen("../Day02/INPUT.txt", "r");
    if (f != NULL)
    {
        Day01(f);
        fclose(f);
    }

    return EXIT_SUCCESS;
}
