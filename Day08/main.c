#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <assert.h>

static void Process(FILE *in)
{
    uint32_t tot_car =0;
    uint32_t tot_mem = 0;

    bool quote = false;
    int skip = 0;

    while (!feof(in))
    {
        int c = fgetc(in);

        if (c == -1)
        {

        }
        else if (c == '\n')
        {

        }
        else 
        {
            tot_car++;

            if (c == '"')
            {
                if (quote)
                {
                    if (skip > 0)
                    {
                        skip --;
                    }
                    else
                    {
                        quote = false;
                    }
                }
                else
                {
                    quote = true;
                }
            }
            else if (c == '\\')
            {
                if (skip > 0)
                {
                    skip--;
                }
                else
                {
                    tot_mem++;
                    skip = 1;
                }
            }
            else if (c == 'x')
            {
                if (skip == 1)
                {
                    skip = 2;
                }
                else if (skip > 0)
                {
                    skip--;
                }
                else
                {
                    tot_mem++;
                }
            }
            else
            {
                if (skip > 0)
                {
                    skip--;
                }
                else
                {
                    tot_mem++;
                }
            }
        }
    }

    fprintf(stdout, "Total string : %" PRIu32 "\n", tot_car);
    fprintf(stdout, "Total mem    : %" PRIu32 "\n", tot_mem);
    fprintf(stdout, "Result       : %" PRIu32 "\n", tot_car - tot_mem);
}


static void Process2(FILE *in)
{
    uint32_t tot_car =0;
    uint32_t tot_mem = 0;

    bool quote = false;
    int skip = 0;

    while (!feof(in))
    {
        int c = fgetc(in);

        if (c == -1)
        {

        }
        else if (c == '\n')
        {
            tot_mem += 2;
        }
        else 
        {
            tot_car++;

            if (c == '"')
            {
                tot_mem+=2;
            }
            else if (c == '\\')
            {
                tot_mem+=2;
            }
            else
            {
                tot_mem++;
            }
        }
    }

    fprintf(stdout, "Total string : %" PRIu32 "\n", tot_car);
    fprintf(stdout, "Total mem    : %" PRIu32 "\n", tot_mem);
    fprintf(stdout, "Result       : %" PRIu32 "\n", tot_mem - tot_car);
}

int main(int, char**)
{
    printf("Hello, from AventOfCode!\n");

    FILE * f = fopen("../Day08/test1.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }

    f = fopen("../Day08/INPUT.txt", "r");
    if (f != NULL)
    {
        Process2(f);
        fclose(f);
    }

    return EXIT_SUCCESS;
}
